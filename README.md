## Welcome

Hello! This is KazSpace where I write about the stuff I like.

Muzyczna Biblioteka Madrego Czlowieka

JAZZ
By all means I am not the jazz expert, I do not even claim to know jazz much, but I listen to it often and I do enjoy it a lot.

You can start with Kind Of Blue but I recommend to start from the end really with excellent 8CD introduction to Miles called The Last Word - The Warner Bros. Years. It is not the best of, it is just a summary of the period of his musical life.

## Jazz Albums I wish to have in my collection:

Ramsey Lewis – Maiden Voyage - https://tidal.com/browse/album/35657133
