### Motto

> Sam na sam z myślami, deszcz oczyścił powietrze
Spacer ze słuchawkami, Miles gra, ale ja myślę
Czy zatrzymanie czasu byłoby dobre faktycznie


> Being on my own with my thoughts, rain cleared up the air
walking with Miles on my headphones, wondering if
stopping time would do us any good

Eldo: Czas

## List of Jazz albums

Some albums may have a short note attached where some may not. Do not look at it as definitive jazz albums list or the best of jazz list. It is a log of the albums, which I enjoyed.

Syntax: [artist] [album] [year of inital release]

1. Miles Davis - Kind of Blue 
It have been my first album I have listened to. Was curious about the song lyrics, which you can see in the motto.
https://en.wikipedia.org/wiki/Kind_of_Blue

2. Gyakuten Saiban Jazz Album ~GYAKUTEN MEETS JAZZ SOUL~
https://www.discogs.com/Masakazu-Sugimori-Akemi-Kimura-Noriyuki-Iwadare-Toshihiko-Horiyama-Hideki-Okugawa-Gyakuten-Saiban-Me/release/12848011

3. Brad Mehldau - Songs: The Art of the Trio Volume Three
https://en.wikipedia.org/wiki/Songs:_The_Art_of_the_Trio_Volume_Three

4. Miles Davis - Ascenseur pour l'échafaud
Beautfiul film and great soundtrack by Miles Davis.
https://en.wikipedia.org/wiki/Ascenseur_pour_l%27%C3%A9chafaud_(soundtrack)

5. Bill Evans - On Green Dolphin Street 
"The title is taken from the 1947 MGM movie Green Dolphin Street and the film's title song". Did not see the film, I may try to watch it.
https://en.wikipedia.org/wiki/On_Green_Dolphin_Street_(Bill_Evans_album)

6. The Roy Hargrove Quintet  - Earfood
Strasbourg / St. Denis is the song which became my instant favourite.
https://www.discogs.com/Roy-Hargrove-Quintet-Earfood/release/2209558

7. Bill Evans - You must believe in spring
https://en.wikipedia.org/wiki/You_Must_Believe_in_Spring_(Bill_Evans_album)

8. Chet Baker - Someday My Prince Will Come 
https://en.wikipedia.org/wiki/Someday_My_Prince_Will_Come_(Chet_Baker_album)

9. Gerry Mulligan - Night Lights
Absolutely stunning album cover.
https://en.wikipedia.org/wiki/Night_Lights_(Gerry_Mulligan_album)

10. Keith Jarrett - The Köln Concert
https://en.wikipedia.org/wiki/The_K%C3%B6ln_Concert

11. Matthew Halsall - Colour Yes
https://www.discogs.com/Matthew-Halsall-Colour-Yes/release/1997124

12. Miles Davis - A Tribute To Jack Johnson
Miles tribute to the heavyweight champion Jack Johnson.
https://www.discogs.com/Miles-Davis-A-Tribute-To-Jack-Johnson/release/1941575

13. Miles Davis - 'Round About Midnight
https://www.discogs.com/Miles-Davis-Round-About-Midnight/master/9130

14. Miles Davis - Someday My Prince Will Come
https://www.discogs.com/Miles-Davis-Sextet-Someday-My-Prince-Will-Come/master/122262

15. Miles Davis - Tutu
https://www.discogs.com/Miles-Davis-Tutu/master/63355

16. Miles Davis - Time After Time
https://www.discogs.com/Miles-Davis-Time-After-Time/master/203873

17. Ryo Fukui - Scenery
https://www.discogs.com/Ryo-Fukui-Scenery/master/509654

18. Tomasz Stanko - Chameleon
https://www.discogs.com/Tomasz-Sta%C5%84ko-Chameleon/master/505945

19. Tomasz Stanko New York Quartet - Wisława
https://www.discogs.com/Tomasz-Stanko-New-York-Quartet-Wis%C5%82awa/release/4272664

20. Weather Report - Heavy Weather
https://www.discogs.com/Weather-Report-Heavy-Weather/master/21748

21. Chet Baker – Chet
https://www.discogs.com/Chet-Baker-Chet/master/60238

22. E.S.T. Live in London
https://en.wikipedia.org/wiki/E.S.T._Live_in_London

23. Guru - Guru's Jazzmatazz, Vol. 1
https://en.wikipedia.org/wiki/Guru%27s_Jazzmatazz,_Vol._1

24. John Coltrane - A Love Supreme
https://en.wikipedia.org/wiki/A_Love_Supreme

25. John Coltrane - Blue Train
https://en.wikipedia.org/wiki/Blue_Train_(album)

26. Ella Fitzgerald - Whisper Not
https://en.wikipedia.org/wiki/Whisper_Not_(Ella_Fitzgerald_album)

27. Thelonious Monk - Solo Monk
https://en.wikipedia.org/wiki/Solo_Monk

28. Krzysztof Komeda – Knife In The Water
https://www.discogs.com/Krzysztof-Komeda-Knife-In-The-Water-Music-From-The-Roman-Polanski-Film/release/4997179

29. Chet Baker Quartet – No Problem 
https://www.discogs.com/Chet-Baker-Quartet-No-Problem/master/284075

30. Chet Baker & Art Pepper – Playboys 
https://www.discogs.com/Chet-Baker-Art-Pepper-Playboys/master/269012

31. Dusko Goykovich – Munich Serenade 
https://www.discogs.com/Dusko-Goykovich-Munich-Serenade/release/7618309

32. Duke Ellington Meets Coleman Hawkins – Duke Ellington Meets Coleman Hawkins 
https://www.discogs.com/Duke-Ellington-Meets-Coleman-Hawkins-Duke-Ellington-Meets-Coleman-Hawkins/master/64983

33. Art Blakey & The Jazz Messengers – A Night In Tunisia 
https://www.discogs.com/Art-Blakey-The-Jazz-Messengers-A-Night-In-Tunisia/master/176732

34. George Benson – Give Me The Night 
https://www.discogs.com/George-Benson-Give-Me-The-Night/master/52671

35. Harold Faltermeyer – Jack Orlando (Game Soundtrack) 
https://www.discogs.com/Harold-Faltermeyer-Jack-Orlando-The-Original-Soundtrack/release/5854412

36. This Is The Police game soundtrack
https://bossbattlerecords.bandcamp.com/album/this-is-the-police-deluxe-edition

37. Charlie Haden – The Best Of Quartet West 
https://www.discogs.com/Charlie-Haden-The-Best-Of-Quartet-West/master/1291927

38. Marvin Hamlisch – The Sting film soundtrack
https://www.discogs.com/Marvin-Hamlisch-The-Sting-Original-Motion-Picture-Soundtrack/master/90923

39. Andrzej Jagodziński Trio – Chopin Metamorphosis 
https://www.discogs.com/Andrzej-Jagodzi%C5%84ski-Trio-Chopin-Metamorphosis/release/8327522

40. Charlie Parker – Charlie Parker Jam Session 
https://www.discogs.com/Charlie-Parker-Charlie-Parker-Jam-Session/release/3701440

41. Count Basie And His Orchestra – April In Paris 
https://www.discogs.com/Count-Basie-And-His-Orchestra-April-In-Paris/release/5147760

42. Django Reinhardt – The Best Of 
https://www.discogs.com/Django-Reinhardt-The-Best-Of/master/537876

43. Herbie Hancock – Maiden Voyage 
https://www.discogs.com/Herbie-Hancock-Maiden-Voyage/release/369203?ev=rr

44. Freddie Hubbard – Open Sesame 
https://www.discogs.com/Freddie-Hubbard-Open-Sesame/master/177623

45. Grant Green – Idle Moments 
https://www.discogs.com/Grant-Green-Idle-Moments/master/163276

46. Gato Barbieri – Caliente! 
https://www.discogs.com/Gato-Barbieri-Caliente/master/178371

47. Manu Katché – Neighbourhood 
https://www.discogs.com/Manu-Katch%C3%A9-Neighbourhood/master/262498

48. Łukasz Ojdana – Kurpian Songs & Meditations 
https://www.discogs.com/%C5%81ukasz-Ojdana-Kurpian-Songs-Meditations/release/14837890

49. Yelena Eckemoff, Manu Katché – Colors 
https://www.discogs.com/Yelena-Eckemoff-Manu-Katch%C3%A9-Colors/release/13350832

50. Lalo Schifrin With Ray Brown & Grady Tate & The London Philharmonic* – Jazz Meets The Symphony 
https://www.discogs.com/Lalo-Schifrin-With-Ray-Brown-Grady-Tate-The-London-Philharmonic-Jazz-Meets-The-Symphony-/release/4281445
